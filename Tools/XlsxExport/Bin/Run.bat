@echo off
XlsxToLua.exe ../../../Numberic ../../../../Client/Program/Lua/src/config/database ../../../../Client/Program/Assets lang.txt -columnInfo -allowedNullNumber -printEmptyStringWhenLangNotMatching -exportIncludeSubfolder
set errorLevel = %errorlevel%
if errorLevel == 0 (
	@echo 导出成功
	lua LuaDataOptimizer.lua
	@echo lua table 优化导出成功
) else (
	@echo 导出失败
)
pause